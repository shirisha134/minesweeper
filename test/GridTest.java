import org.junit.*;

import java.util.ArrayList;

public class GridTest {
  @Test
  public void shouldBeAbleToCreateAGridWithSquareCellsWithItsNeighbouringMines(){
    Cell cell1=new Cell(1,1);
    ArrayList<Cell> cell1Neighbours=new ArrayList<>();
    cell1Neighbours.add(new Cell(1,2));
    cell1Neighbours.add(new Cell(2,1));

    Cell cell2 = new Cell(0,0);
    ArrayList<Cell> cell2Neighbours=new ArrayList<>();
    cell2Neighbours.add(new Cell(1,0));
    cell2Neighbours.add(new Cell(1,1));
    cell2Neighbours.add(new Cell(0,1));

    Square square1=new Square(cell1,cell1Neighbours);
    Square square2=new Square(cell2,cell2Neighbours);

    ArrayList<Square> listOfSquares=new ArrayList<>();
    listOfSquares.add(square1);
    listOfSquares.add(square2);
    Grid grid =new Grid(listOfSquares);

    Assert.assertEquals(2,grid.getSize());
  }
  @Test
  public void shouldBeAbleToReturnNewGridWithUpdatesSquareCellStatus(){
    Cell cell1=new Cell(0,0);
    ArrayList<Cell> cell1Neighbours=new ArrayList<>();
    cell1Neighbours.add(new Cell(1,0));
    cell1Neighbours.add(new Cell(0,1));

    Cell cell2 = new Cell(2,2);
    ArrayList<Cell> cell2Neighbours=new ArrayList<>();
    cell2Neighbours.add(new Cell(2,1));
    cell2Neighbours.add(new Cell(1,2));

    Square square1=new Square(cell1,cell1Neighbours);
    Square square2=new Square(cell2,cell2Neighbours);

    ArrayList<Square> listOfSquares=new ArrayList<>();
    listOfSquares.add(square1);
    listOfSquares.add(square2);
    Grid grid =new Grid(listOfSquares);

    Grid actualGrid=grid.whenClicked(cell1);

    Cell cell3=new Cell(0,0);
    ArrayList<Cell> cell3Neighbours=new ArrayList<>();
    cell3Neighbours.add(new Cell(0,1));
    cell3Neighbours.add(new Cell(1,0));

    Cell cell4 = new Cell(2,2);
    ArrayList<Cell> cell4Neighbours=new ArrayList<>();
    cell4Neighbours.add(new Cell(2,1));
    cell4Neighbours.add(new Cell(1,2));

    Square square3=new Square(cell3,"2",cell3Neighbours);
    Square square4=new Square(cell4,"X",cell4Neighbours);
    ArrayList<Square> listOfExpectedSquares=new ArrayList<>();
    listOfExpectedSquares.add(square3);
    listOfExpectedSquares.add(square4);
    Grid expectedGrid =new Grid(listOfExpectedSquares);

    Assert.assertEquals(expectedGrid,actualGrid);
  }


}
