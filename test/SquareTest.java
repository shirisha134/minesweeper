import org.junit.*;
import org.junit.Assert;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SquareTest {
  @Test
  public void shouldReturnASquareWithZeroNeighbouringMines() {
    Cell cell = new Cell(1, 1);
    Square square = new Square(cell);
    assertEquals(new Square(cell,"0"), square.onClick(cell));
  }

  @Test
  public void shouldShowOneOnClickWhenHaveOneNeighbouringMine() {
    Cell cell1 = new Cell(1, 1);
    ArrayList<Cell> neighbouringMines = new ArrayList<>();
    Cell cell2 = new Cell(1, 2);
    neighbouringMines.add(cell2);
    Square square = new Square(cell1, neighbouringMines);
    assertEquals(new Square(cell1,"1"), square.onClick(cell1));
  }
  @Test
  public void shouldFailOnClickingTheSquareHavingMine() {
    Cell cell = new Cell(1, 1);
    ArrayList<Cell> neighbouringMines = new ArrayList<>();
    neighbouringMines.add(cell);
    Square square = new Square(cell, neighbouringMines);
    assertEquals(new Square(cell,"Oops!"), square.onClick(cell));
  }

  @Test
  public void ShouldBeAbleToFlagASquareCell(){
    Cell cell =new Cell(1,1);
    ArrayList<Cell> neighbouringMines = new ArrayList<>();
    Square square = new Square(cell,neighbouringMines);
    assertEquals(new Square(cell,"f"),square.onFlag(cell));
  }


}
