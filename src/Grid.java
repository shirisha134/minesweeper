import java.util.ArrayList;

public class Grid {

  private ArrayList<Square> listOfSquares;

  public Grid(ArrayList<Square> listOfSquares) {
    this.listOfSquares = listOfSquares;
  }

  public int getSize() {
    return listOfSquares.size();
  }

  public Grid whenClicked(Cell cell) {
    ArrayList<Square> newListOfSquares=new ArrayList<>();

    for(Square square:listOfSquares){
      if(square.has(cell)){

        Square newSquare =square.onClick(cell);
        newListOfSquares.add(newSquare);
      }
      newListOfSquares.add(square);
    }
    return new Grid(newListOfSquares);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Grid grid = (Grid) o;

    return listOfSquares != null ? listOfSquares.equals(grid.listOfSquares) : grid.listOfSquares == null;

  }

  @Override
  public int hashCode() {
    return listOfSquares != null ? listOfSquares.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Grid{" +
        "listOfSquares=" + listOfSquares +
        '}';
  }
}
