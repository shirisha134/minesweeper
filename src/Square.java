import java.util.ArrayList;

public class Square {
  private Cell cell;
  private String status = "X";
  private ArrayList<Cell> neighbourWithMine = new ArrayList<>();

  public Square(Cell cell, ArrayList<Cell> neighbourWithMine) {
    this.cell = cell;
    this.neighbourWithMine = neighbourWithMine;
  }

  public Square(Cell cell, String status) {
    this.cell = cell;
    this.status = status;
  }
  public Square(Cell cell, String status,ArrayList<Cell> neighbourWithMine) {
    this.cell = cell;
    this.status = status;
    this.neighbourWithMine=neighbourWithMine;
  }

  public Square(Cell cell) {
    this.cell = cell;
  }


  public Square onClick(Cell cell) {
    if (neighbourWithMine.contains(cell)) {
      return new Square(cell, "Oops!",neighbourWithMine);
    }
    return new Square(cell, String.valueOf(neighbourWithMine.size()),neighbourWithMine);
  }

  public Square onFlag(Cell cell) {
    return new Square(cell,"f",neighbourWithMine);
  }

  public boolean has(Cell cell) {
    return this.cell.equals(cell);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Square square = (Square) o;

    return status != null ? status.equals(square.status) : square.status == null;

  }

  @Override
  public int hashCode() {
    return status != null ? status.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Square{" +
        "cell=" + cell +
        ", status='" + status + '\'' +
        ", neighbourWithMine=" + neighbourWithMine +
        '}';
  }
}
