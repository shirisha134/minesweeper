import java.util.ArrayList;

public class Cell {
  private int x;
  private int y;
  private static ArrayList<Cell> directions;

  Cell(int x, int y) {
    this.x = x;
    this.y = y;
  }

  private static void addDirections() {
    directions.add(new Cell(0, -1));
    directions.add(new Cell(1, 0));
    directions.add(new Cell(0, 1));
    directions.add(new Cell(-1, 0));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Cell cell = (Cell) o;

    if (x != cell.x) return false;
    return y == cell.y;

  }

  @Override
  public int hashCode() {
    int result = x;
    result = 31 * result + y;
    return result;
  }

  public ArrayList<Cell> neighbours() {
    return  directions;
  }
}
